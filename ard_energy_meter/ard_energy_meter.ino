/*
  EmonTx CT123 Voltage Serial Only example
  
  Part of the openenergymonitor.org project
  Licence: GNU GPL V3
  
  Author: Trystan Lea
  
  Added WiFi support with ESP8266
  Added EEPROM config
  Author: Dimitris Tassopoulos
*/
#include <EEPROM.h>
#include <SoftwareSerial.h>
#include "EmonLib.h"
#include <stdio.h>

#define AP_SSID			""		// use your AP
#define AP_PASSWD		""		// use your AP wpa/wpa2 password
#define AP_SERVER_IP	""	// use your own ip
#define AP_SERVER_PORT	7710

#define LPF_SLOW	0		// slow pass filter
#define LPF_FAST	1		// fast pass filter

#define DEBUG_WIFI

#ifdef DEBUG_WIFI
	#define DEBUG(X)		if (dbg_print) Serial.print(X)
	#define DEBUGL(X)		if (dbg_print) Serial.println(X)
	#define DEBUGF(X)		if (dbg_print) Serial.print(F(X))
	#define DEBUGFL(X)		if (dbg_print) Serial.println(F(X))
#else
	#define DEBUG(X)		{}
	#define DEBUGL(X)		{}
	#define DEBUGF(X)		{}
	#define DEBUGFL(X)		{}
#endif

// EEPROM config structure
//
#pragma pack(1)
typedef struct {
	uint16_t	eeprom_header;
	char		wifi_ssid[40];
	char		wifi_passwd[32];
	char		udp_ip[16];
	uint16_t	port;
	uint16_t	udp_periodic_send;	// in seconds
	uint8_t		lpf_mode;
	uint16_t	eeprom_cs;
} tp_conf;
tp_conf conf;


// UDP packet
//
typedef struct {
	double	irms;
	double	realPower;
	double	aparentPower;
	double	powerFactor;
} tp_ct_val;

typedef struct {
	double vrms;
	tp_ct_val	ct1;
	tp_ct_val	ct2;
	tp_ct_val	ct3;
} tp_udp_packet;

tp_udp_packet	glb_udp_packet;

#define CMD_BUFF_LEN	100
char cmd_buff[CMD_BUFF_LEN];
int cmd_len = 0;
uint8_t dbg_print = 0;
uint8_t conf_mode = 0;
unsigned long start = millis();
uint16_t samples = 0;

EnergyMonitor ct1,ct2,ct3;

const int LEDpin = 9;

// Software serial port (leave Serial0 for debuging)
SoftwareSerial wifi(2, 13);	// RX,TX

void setup() 
{
	wifi.begin(9600);	// Start soft/wifi uart
	
	Serial.begin(9600);
	Serial.println(F("Jaco's Energy Meter")); 
	Serial.println(F("With emonTx shield from OpenEnergyMonitor.org"));
	
	CONF_Read();		// Read config from EEPROM
	
	ct1.current(1, 58);	//59.206
	ct2.current(2, 58);
	ct3.current(3, 58);

	ct1.voltage(0, 212.643, 1.25);	//230 209 215.2 212.569
	ct2.voltage(0, 212.643, 1.7);	//230 215.2 1.35
	ct3.voltage(0, 212.643, 1.7);	//230 15.2 1.45
	
	ESP8266_Init();		// Initialize ESP8266
	
	pinMode(LEDpin, OUTPUT);		// Heartbeat LED
	digitalWrite(LEDpin, HIGH);
	
	// Take first measurement
	ct1.calcVI(20, 2000);
	ct2.calcVI(20, 2000);
	ct3.calcVI(20, 2000);
	
	if (conf.lpf_mode == LPF_FAST) {
		// Init udp data buffers
		glb_udp_packet.vrms = ct1.Vrms;
		// CT1
		glb_udp_packet.ct1.irms = ct1.Irms;
		glb_udp_packet.ct1.realPower = ct1.realPower;
		glb_udp_packet.ct1.aparentPower = ct1.apparentPower;
		glb_udp_packet.ct1.powerFactor = ct1.powerFactor;
		// CT2
		glb_udp_packet.ct2.irms = ct2.Irms;
		glb_udp_packet.ct2.realPower = ct2.realPower;
		glb_udp_packet.ct2.aparentPower = ct2.apparentPower;
		glb_udp_packet.ct2.powerFactor = ct2.powerFactor;
		// CT3
		glb_udp_packet.ct3.irms = ct3.Irms;
		glb_udp_packet.ct3.realPower = ct3.realPower;
		glb_udp_packet.ct3.aparentPower = ct3.apparentPower;
		glb_udp_packet.ct3.powerFactor = ct3.powerFactor;
	}
}

void loop() 
{ 
	if (conf_mode) {
		// Check for serial commands
		if ((cmd_len = Serial.available()) > 0) {
			Serial.readBytesUntil('\n', cmd_buff, CMD_BUFF_LEN);
			HandleSerialCmd(cmd_buff, cmd_len);
			memset(cmd_buff, 0, CMD_BUFF_LEN);
			Serial.flush();
		}
	}
	else {
		// Measure
		ct1.calcVI(20,2000);
		ct2.calcVI(20,2000);
		ct3.calcVI(20,2000);

		// Load values
		if (conf.lpf_mode == LPF_FAST) {
			FilterData_LPF1();	// Just copy ctx values to udp struct
		}
		else {
			FilterData_LPF2();
		}
		

		// Print Values
		PrintValues();	// Comment out if it's not needed
	
		// If connection status is connected then send data
		if (ESP8266_CIPSTATUS()) {
			if ((millis() - start) >= (conf.udp_periodic_send * 1000)) {
				if (conf.lpf_mode == LPF_SLOW) {
					PrepareData_LPF2();
				}
				ESP8266_CIPSENDSingle((uint8_t*) &glb_udp_packet, sizeof(tp_udp_packet));
				start = millis();
				Serial.println(F("===>"));
			}
		}
		// else try reconnect and re-bind to the UDP port
		else {
			ESP8266_Init();
		}
	
		// Check for serial commands
		if ((cmd_len = Serial.available()) > 0) {
			Serial.readBytesUntil('\n', cmd_buff, CMD_BUFF_LEN);
			if (!(strncmp(cmd_buff, "CONF=1", 6))) {
				conf_mode = 1;
			}
			memset(cmd_buff, 0, CMD_BUFF_LEN);
			Serial.flush();
			Serial.println(F("CONF mode enabled"));
		}
	}
}

void HandleSerialCmd(char * data, int len)
{
	int i;
	
	// remove \r\n
	for (i=0; i<len; i++) {
		if ((data[i] == '\r') || (data[i] == '\n')) data[i] = 0;
	} 
	Serial.print("CMD: "); Serial.println(data);
	
	if (!(strncmp(cmd_buff, "CONF=0", 6))) {
		conf_mode = 0;
	}
	else if (!(strncmp(data, "AP_LIST?", 8))) {
		ESP8266_CWLAP();
	}
	else if (!(strncmp(data, "AP_SSID", 7))) {
		if (data[7] == '?') {
			Serial.print(F("AP_SSID=")); Serial.println(conf.wifi_ssid);
		}
		else if (data[7] == '=') {
			strcpy(conf.wifi_ssid, &data[8]);
			Serial.print(F("AP_SSID set to: ")); Serial.println(conf.wifi_ssid);
		}
	}
	else if (!(strncmp(data, "AP_PASSWD", 9))) {
		if (data[9] == '?') {
			Serial.print(F("AP_PASSWD=")); Serial.println(conf.wifi_passwd);
		}
		else if (data[9] == '=') {
			strcpy(conf.wifi_passwd, &data[10]);
			Serial.print(F("AP_PASSWD set to: ")); Serial.println(conf.wifi_passwd);
		}
	}
	else if (!(strncmp(data, "SERVER_IP", 9))) {
		if (data[9] == '?') {
			Serial.print(F("SERVER_IP=")); Serial.println(conf.udp_ip);
		}
		else if (data[9] == '=') {
			strcpy(conf.udp_ip, &data[10]);
			Serial.print(F("SERVER_IP set to: ")); Serial.println(conf.udp_ip);
		}
	}
	else if (!(strncmp(data, "SERVER_PORT", 11))) {
		if (data[11] == '?') {
			Serial.print(F("SERVER_PORT=")); Serial.println(conf.port);
		}
		else if (data[11] == '=') {
			conf.port = atoi((const char*) &data[12]);
			Serial.print(F("SERVER_PORT set to: ")); Serial.println(conf.port);
		}
	}
	else if (!(strncmp(data, "SERVER_POLL", 11))) {
		if (data[11] == '?') {
			Serial.print(F("SERVER_POLL=")); Serial.println(conf.udp_periodic_send);
		}
		else if (data[11] == '=') {
			conf.udp_periodic_send = atoi((const char*) &data[12]);
			Serial.print(F("SERVER_POLL set to: ")); Serial.println(conf.udp_periodic_send);
		}
	}
	else if (!(strncmp(data, "LPF_MODE", 8))) {
		if (data[8] == '?') {
			Serial.print(F("LPF_MODE=")); Serial.println(conf.lpf_mode);
		}
		else if (data[8] == '=') {
			conf.lpf_mode = atoi((const char*) &data[9]);
			Serial.print(F("LPF_MODE set to: ")); Serial.println(conf.lpf_mode);
		}
	}
	else if (!(strncmp(data, "SAVE", 4))) {
		CONF_Write();
		Serial.println(F("Configuration saved in EEPROM"));
	}
	else if (!(strncmp(data, "DEBUG=", 6))) {
		dbg_print = atoi((const char*) &data[6]);
		Serial.print(F("DEBUG set to: ")); Serial.println(dbg_print);
	}
	else if (!(strncmp(data, "DEFAULTS", 8))) {
		CONF_Defaults();
		Serial.println(F("Loaded default configuration in EEPROM"));
	}
}

void PrintValues(void)
{
	Serial.print("Vrms:");
	Serial.println(ct1.Vrms);
	
	Serial.print("CT1:");
	Serial.print(ct1.Irms);
	Serial.print(" - ");
	Serial.print(ct1.realPower);
	Serial.print(" - ");
	Serial.print(ct1.apparentPower);
	Serial.print(" - ");
	Serial.println(ct1.powerFactor);
	
	Serial.print("CT2: ");
	Serial.print(ct2.Irms);
	Serial.print(" - ");
	Serial.print(ct2.realPower);
	Serial.print(" - ");
	Serial.print(ct2.apparentPower);
	Serial.print(" - ");
	Serial.println(ct2.powerFactor);
	
	Serial.print("CT3: ");
	Serial.print(ct3.Irms);
	Serial.print(" - ");
	Serial.print(ct3.realPower);
	Serial.print(" - ");
	Serial.print(ct3.apparentPower);
	Serial.print(" - ");
	Serial.println(ct3.powerFactor);
	
	Serial.println();
}

/**
	@brief This function adds the samples constantly and increments a counter.
		The counter is reset when the UDP packet is sent and the total sum
		for each value is divided by the number of samples.
		This is a slow low pass filter.
*/
void FilterData_LPF2(void)
{
	glb_udp_packet.vrms += ct1.Vrms;
	// CT1
	glb_udp_packet.ct1.irms += ct1.Irms;
	glb_udp_packet.ct1.realPower += ct1.realPower;
	glb_udp_packet.ct1.aparentPower += ct1.apparentPower;
	glb_udp_packet.ct1.powerFactor += ct1.powerFactor;
	// CT2
	glb_udp_packet.ct2.irms += ct2.Irms;
	glb_udp_packet.ct2.realPower += ct2.realPower;
	glb_udp_packet.ct2.aparentPower += ct2.apparentPower;
	glb_udp_packet.ct2.powerFactor += ct2.powerFactor;
	// CT3
	glb_udp_packet.ct3.irms += ct3.Irms;
	glb_udp_packet.ct3.realPower += ct3.realPower;
	glb_udp_packet.ct3.aparentPower += ct3.apparentPower;
	glb_udp_packet.ct3.powerFactor += ct3.powerFactor;	
	samples++;
}

void PrepareData_LPF2(void)
{
	glb_udp_packet.vrms /= samples;
	// CT1
	glb_udp_packet.ct1.irms /= samples;
	glb_udp_packet.ct1.realPower /= samples;
	glb_udp_packet.ct1.aparentPower /= samples;
	glb_udp_packet.ct1.powerFactor /= samples;
	// CT2
	glb_udp_packet.ct2.irms /= samples;
	glb_udp_packet.ct2.realPower /= samples;
	glb_udp_packet.ct2.aparentPower /= samples;
	glb_udp_packet.ct2.powerFactor /= samples;
	// CT3
	glb_udp_packet.ct3.irms /= samples;
	glb_udp_packet.ct3.realPower /= samples;
	glb_udp_packet.ct3.aparentPower /= samples;
	glb_udp_packet.ct3.powerFactor /= samples;
	
	samples = 0;
}

/** 
	@brief This function adds the current and the previous sample and then
		divides the samples by two. This is an aggresive fast low pass filter
*/
void FilterData_LPF1(void)
{
	glb_udp_packet.vrms = (glb_udp_packet.vrms + ct1.Vrms)/2;
	// CT1
	glb_udp_packet.ct1.irms = (glb_udp_packet.ct1.irms + ct1.Irms)/2;
	glb_udp_packet.ct1.realPower = (glb_udp_packet.ct1.realPower + ct1.realPower)/2;
	glb_udp_packet.ct1.aparentPower = (glb_udp_packet.ct1.aparentPower + ct1.apparentPower)/2;
	glb_udp_packet.ct1.powerFactor = (glb_udp_packet.ct1.powerFactor + ct1.powerFactor)/2;
	// CT2
	glb_udp_packet.ct2.irms = (glb_udp_packet.ct2.irms + ct2.Irms)/2;
	glb_udp_packet.ct2.realPower = (glb_udp_packet.ct2.realPower + ct2.realPower)/2;
	glb_udp_packet.ct2.aparentPower = (glb_udp_packet.ct2.aparentPower + ct2.apparentPower)/2;
	glb_udp_packet.ct2.powerFactor = (glb_udp_packet.ct2.powerFactor + ct2.powerFactor)/2;
	// CT3
	glb_udp_packet.ct3.irms = (glb_udp_packet.ct3.irms + ct3.Irms)/2;
	glb_udp_packet.ct3.realPower = (glb_udp_packet.ct3.realPower + ct3.realPower)/2;
	glb_udp_packet.ct3.aparentPower = (glb_udp_packet.ct3.aparentPower + ct3.apparentPower)/2;
	glb_udp_packet.ct3.powerFactor = (glb_udp_packet.ct3.powerFactor + ct3.powerFactor)/2;
}

void CONF_Write(void)
{
	uint8_t i;
	uint8_t * p = (uint8_t*) &conf;
	
	conf.eeprom_header = 0xABCD;
	conf.eeprom_cs = 0;	// reset checksum
	// Calculate checksum
	for (i=0; i<sizeof(tp_conf)-2; i++) {
		conf.eeprom_cs += p[i];
	}
	Serial.println(F("EEPROM: "));
	// Save to EEPROM
	for (i=0; i<sizeof(tp_conf); i++) {
		EEPROM.write(i, p[i]);
		Serial.print(p[i], HEX); Serial.print(",");
	}
	Serial.println();
}

void CONF_Defaults(void)
{
	uint8_t i;
	uint8_t * p = (uint8_t*) &conf;
	
	// first time used. Load defaults
	conf.eeprom_header = 0xABCD;
	memset(conf.wifi_ssid, 0, sizeof(conf.wifi_ssid)/sizeof(conf.wifi_ssid[0]));
	memset(conf.wifi_passwd, 0, sizeof(conf.wifi_passwd)/sizeof(conf.wifi_passwd[0]));
	memset(conf.udp_ip, 0, sizeof(conf.udp_ip)/sizeof(conf.udp_ip[0]));
	strcpy(conf.wifi_ssid, AP_SSID);
	strcpy(conf.wifi_passwd, AP_PASSWD);
	strcpy(conf.udp_ip, AP_SERVER_IP);
	conf.port = AP_SERVER_PORT;
	conf.udp_periodic_send = 10;
	conf.lpf_mode = LPF_SLOW;
	conf.eeprom_cs = 0;	// reset checksum
	for (i=0; i<sizeof(tp_conf)-2; i++) {
		conf.eeprom_cs += p[i];
	}
	// Save to EEPROM
	for (i=0; i<sizeof(tp_conf); i++) {
		EEPROM.write(i, p[i]);
	}
}

/**
* Read configuration data from EEPROM and if they don't exists then
* load defaults.
*/
void CONF_Read(void)
{
	uint8_t i;
	uint8_t * p = (uint8_t*) &conf;
	uint8_t valid_data = false;
	
	Serial.print(F("EEPROM content:"));
	for (i=0; i<sizeof(tp_conf); i++) {
		p[i] = EEPROM.read(i);
		Serial.print(p[i], HEX); Serial.print(",");
	}
	
	if (conf.eeprom_header == 0xABCD) {
		// probably valid data
		uint16_t cs = 0;
		for (i=0; i<sizeof(tp_conf)-2; i++) {
			cs += p[i];
		}
		Serial.print(F("Checksum: ")); Serial.print(cs);
		Serial.print(F(" / ")); Serial.println(conf.eeprom_cs);
		if (cs == conf.eeprom_cs) {
			//valid data
			valid_data = true;
		}
	}
	
	// if not valid data found or false cs the load defaults
	if (!valid_data) {
		CONF_Defaults();
	}
	
	Serial.print("header: ");Serial.println(conf.eeprom_header);
	Serial.print("ssid: ");Serial.println(conf.wifi_ssid);
	Serial.print("pswd: ");Serial.println(conf.wifi_passwd);
	Serial.print("udp ip: ");Serial.println(conf.udp_ip);
	Serial.print("port: ");Serial.println(conf.port);
	Serial.print("per send: ");Serial.println(conf.udp_periodic_send);
	Serial.print("lpf mode: ");Serial.println(conf.lpf_mode);
}

/**
* Initialize ESP8266, connect to AP and bind a UDP port
*/
uint8_t ESP8266_Init(void)
{
	uint8_t result = 0;	// by default no wifi
	
	//Init ESP8266
	Serial.println(F("Init WiFi"));
	rx_empty();
	if (ESP8266_RST()) {
		bool resp = false;
		
		Serial.println(F("Reseting WiFi"));
		while (!ESP8266_AT()) {
			Serial.println(F("WiFi timed out w/o OK"));
			delay(200);
		}
		
		//ESP8266_CWLAP();
		
		if (strlen(conf.wifi_ssid) <= 1) {
			Serial.println(F("SSID is not set, abort WiFi init"));
			return 0;
		}
		
		resp = ESP8266_CWMODE(3);
		if (resp) {
			Serial.println(F("WiFi: Set to mode 3 "));
		}
		delay(200);
				
		
		resp = ESP8266_CWJAP(conf.wifi_ssid, conf.wifi_passwd);
		if (resp) {
			Serial.println(F("WiFi: Connected AP "));
		}
		delay(200);
		
		ESP8266_CIPMUX(0);
		if (resp) {
			Serial.println(F("WiFi: Set mux to 0 "));
		}
		delay(200);
		
		ESP8266_CIPSTART(conf.udp_ip, conf.port);
		if (resp) {
			Serial.println(F("WiFi: UDP port bind"));
		}
		delay(200);
		result = 1;
	}
	else {
		Serial.println(F("WiFi not found..."));
		delay(200);
	}
	return(result);
}

void rx_empty(void)
{
	while(wifi.available() > 0) {
		wifi.read();
	}
}

String recvString(String target, uint32_t timeout = 1000)
{
	String data;
	char a;
	unsigned long start = millis();
	while (millis() - start < timeout) {
		while(wifi.available() > 0) {
			a = wifi.read();
			if(a == '\0') continue;
			data += a;
		}
		if (data.indexOf(target) != -1) {
			break;
		}
	}
	// Debug print received data
	DEBUGF("R:"); DEBUGL(data);
	return data;
}

bool recvFind(String target, uint32_t timeout)
{
	String data_tmp;
	data_tmp = recvString(target, timeout);
	if (data_tmp.indexOf(target) != -1) {
		return true;
	}
	return false;
}

bool ESP8266_AT(void)
{
	rx_empty();
	wifi.println("AT");
	return recvFind("OK", 1000);
}

bool ESP8266_RST(void)
{
	rx_empty();
	wifi.println("AT+RST");
	return recvFind("Ready", 10000);
}

bool ESP8266_CWMODE(int mode)
{
	String data;
	
	rx_empty();
	wifi.print("AT+CWMODE=");
	wifi.println(mode);
	
	data = recvString("OK", 3000);
	if (data.indexOf("OK") != -1) {
		return true;
	}
	return false;
}

//AT+CWLAP
bool ESP8266_CWLAP(void)
{
	String data;
	rx_empty();
	wifi.println("AT+CWLAP");
	
	data = recvString("OK", 10000);
	if (data.indexOf("OK") != -1) {
		Serial.println(data);
		return true;
	}
	return false;
}

bool ESP8266_CWJAP(String ssid, String pwd)
{
	String data;
	rx_empty();
	wifi.print("AT+CWJAP=\"");
	wifi.print(ssid);
	wifi.print("\",\"");
	wifi.print(pwd);
	wifi.println("\"");
	
	data = recvString("OK", 10000);
	if (data.indexOf("OK") != -1) {
		return true;
	}
	return false;
}

bool ESP8266_CIPMUX(int mux)
{
	String data;
	rx_empty();
	wifi.print("AT+CIPMUX=");
	wifi.println(mux);
	
	data = recvString("OK", 3000);
	if (data.indexOf("OK") != -1) {
		return true;
	}
	return false;
}

bool ESP8266_CIPSTART(String ip, int port)
{
	String data;
	rx_empty();
	wifi.print("AT+CIPSTART=\"UDP\",\"");
	wifi.print(ip);
	wifi.print("\",");
	wifi.println(port);
	
	data = recvString("OK", 3000);
	if (data.indexOf("OK") != -1) {
		return true;
	}
	return false;
}

bool ESP8266_CIPSTATUS(void)
{
	String data;
	bool result = false;
	
	delay(100);
	rx_empty();
	wifi.println("AT+CIPSTATUS");
	data = recvString("\r\n\r\nOK");
	int index = data.indexOf("+CIPSTATUS:");
	DEBUGF("index = "); DEBUGL(index);
	index = data.indexOf("STATUS:");
	DEBUGF("index = "); DEBUGL(index);
	if (index) {
		int status = data.substring(index+7, index+7+1).toInt();
		DEBUGF("UDP status = "); DEBUGL(status);
		if (status == 5) {
			result = true;
		}
	}
	return(result);
}

bool ESP8266_CIPSENDSingle(const uint8_t *buffer, uint32_t len)
{
	rx_empty();
	wifi.print("AT+CIPSEND=");
	wifi.println(len);
	if (recvFind(">", 5000)) {
		rx_empty();
		for (uint32_t i = 0; i < len; i++) {
			wifi.write(buffer[i]);
		}
		return recvFind("SEND OK", 10000);
	}
	return false;
}
